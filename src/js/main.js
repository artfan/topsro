 $('.slider__wrapper').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  speed: 500,
  dots: false,
  asNavFor: '.slider__pagination',
  // dotsClass
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        dots: true,
        // appendDots: $('.slider__dots')
        // dotsClass: ("slider__dots"),
      }
    },
  ]
});
$('.slider__pagination').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slider__wrapper',
  dots: false,
  centerMode: true,
  focusOnSelect: true
});

// mobile menu
function openMobile(){
  $(this).toggleClass("mobile-menu--open");
  $(".main-nav").toggleClass("main-nav--m-open");
}
$(".mobile-menu").click(openMobile);